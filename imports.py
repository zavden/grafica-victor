from platform import version
from manimlib import *


class SetLinesNumbers(VGroup):
    CONFIG = {
        "divisions": 10,
        # decimal_number_config
        "dnc": {
            "font_size": 15
        },
        # decimal_number_position_config
        "dnpc": {
            "direction": LEFT,
            "buff": 0.1
        },
        "break_down_value": 1.25,
        "break_up_value": 1.5,
        "lines_height": 2
    }
    def __init__(self, nl: NumberLine, initial_value=20, **kwargs):
        digest_config(self, kwargs)
        self.initial_value = initial_value
        self.lines = self.get_initial_lines(nl)
        self.numbers = self.get_initial_numbers(nl ,self.lines, initial_value)
        super().__init__(self.lines, self.numbers)


    def check_if_change_values(self, nv):
        iv = self.initial_value
        v = iv / nv

        if v >= self.break_down_value or (1/v) >= self.break_up_value:
            return v
        else:
            return 1


    def set_next_value(self, nl: NumberLine, next_value):
        change = self.check_if_change_values(next_value)
        print(f"iv = {self.initial_value}")
        print(f"nv = {next_value}")
        print(f"pre_change = {change}")
        if change >= self.break_down_value:
            change = np.ceil(change)
        elif change <= self.break_up_value:
            tmp_change = 1 / change
            tmp_change = np.ceil(tmp_change)
            change = 1 / tmp_change
        print(f"pos_change = {change}")
        print("-"*40)
        nnl = NumberLine(
            x_range=[0,next_value,1],
            width=nl.get_width(),
            include_ticks=False,
        )
        nnl.move_to(nl)
        self.lines.generate_target()
        lines_c = self.lines.target

        for l,n in zip(lines_c,self.numbers):
            l.set_x(nnl.n2p(n.get_value()/change)[0])
            if l.get_x() > nnl.get_end()[0]+0.1:
                l.set_style(stroke_opacity=0)
            else:
                l.set_style(stroke_opacity=1)
        if change == 1:
            return MoveToTarget(self.lines)
        else:
            # self.initial_value = max(next_value,self.initial_value/change)
            self.initial_value /= change
            return AnimationGroup(
                MoveToTarget(self.lines),
                *[
                    ChangeDecimalToValue(d,d.get_value()/change)
                    for d in self.numbers
                ]
            )


    def get_initial_lines(self, nl: NumberLine):
        return VGroup(*[
            Line(
                nl.n2p((i+1)*nl.x_max/self.divisions),
                nl.n2p((i+1)*nl.x_max/self.divisions)+UP*self.lines_height,
            )
            for i in range(self.divisions)
        ])


    def get_initial_numbers(self, nl, lines, iv):
        numbers = VGroup()
        self.initial_number_width = 1
        for i,l in zip(range(self.divisions),lines):
            n = DecimalNumber((i+1)*iv/self.divisions,**self.dnc)
            n.aspect_ratio = n.get_width() / n.get_height()
            # n.initial_width = n.get_width()
            n.add_updater(self.get_updater_to_number(nl,l,lines))
            numbers.add(n)
        return numbers


    def get_updater_to_number(self, nl: NumberLine, line: Line, lines):
        def update_(mob: DecimalNumber):
            # line_buff = abs(lines[0].get_x()-lines[1].get_x())-0.2
            # if mob.get_width() < line_buff:
            #     mob.set_height()
            mob.next_to(line, **self.dnpc)
            mob.align_to(line,DOWN)
            if line.get_x() > nl.get_end()[0]+0.1:
                mob.set_style(fill_opacity=0)
            else:
                mob.set_style(fill_opacity=1)
        return update_


def get_data_from_csv(file_name):
    import csv
    coords = []
    with open(f'./assets/csv/{file_name}.csv', 'r') as csvFile:
        reader = list(csv.reader(csvFile))
        values = [float(v) for v in reader[0][1:]]
        for row in reader[1:]:
            label,*data = row
            data = [float(d.replace('"',"")) for d in data]
            coords.append([label, data])
    csvFile.close()
    return [values,coords]


class SortCharPlot(VGroup):
    def __init__(self, ha,csv_name="Libro1formato",**kwargs):
        self.set_axes(ha)
        values, coords  = get_data_from_csv(csv_name)

        n_data = len(coords)
        dots_for_labels = VGroup(*[
            Dot(fill_opacity=0).move_to(
                self.va.point_from_proportion(
                    (i / n_data) + (1 / n_data) * 0.5
                )
            )
            for i in range(n_data)
        ])[::-1]

        v = self.get_sort_values(coords)
        print(np.array(v))
        initial_state = self.get_chart_from_single_value(v[0], dots_for_labels)
        self.ist = initial_state
        self.values = v
        self.dfl = dots_for_labels

        super().__init__(
            self.va,
            self.ha,
            dots_for_labels,
            initial_state
        )


    def get_sort_values(self, coords):
        vertical_values = []
        for i in range(len(coords[0][1])):
            tmp = []
            for j,c in enumerate(coords):
                tmp.append([c[0],c[1][i],j])
            tmp2 = sorted(tmp, key=lambda x: x[1],reverse=True)
            vertical_values.append(tmp2)
        return vertical_values


    def get_chart_from_single_value(self, value, dfl):
        max_value = value[0][1]
        # print(max_value)
        grp = VGroup()
        lw = self.ha.get_width()
        enum = 0
        for v,d in zip(value, dfl):
            t = Text(v[0]).scale(0.5)
            t.next_to(d,LEFT)
            # print(v[1]/max_value)
            chart = Rectangle(
                height=t.get_height(),
                width=lw*(v[1]/max_value),
                fill_opacity=1,
            )
            chart.next_to(d.get_center(),RIGHT,buff=0)
            g = VGroup(t,chart)
            g.id =[v[2],enum]
            grp.add(g)
            # print(grp.id)
            enum +=1
        return grp


    def transform_to_next_value(self, next_index):
        state = self.ist
        values = self.values
        dfln = self.dfl
        c = values[next_index]
        max_value = c[0][1]
        print(f"max_value = {max_value}")
        targets = []
        initials = []
        for i,j in zip(state,c):
            print(f"{i.id} -> {[j[2],i.id[1]]}")
            targets.append([j[2],i.id[1]])
            initials.append(i.id)
        targets.sort()
        initials.sort()
        print(targets)
        print(initials)
        for i,mob in enumerate(state):
            mob.generate_target()
            t = mob.target
            ni = self.find_id_target(targets,t)
            # print(ni)
            t.set_y(dfln[ni].get_y())
            if ni == 0:
                # t[1].set_color(RED)
                t[1].set_width(self.ha.get_width(),stretch=True)
                t[1].next_to(dfln[ni].get_center(),RIGHT,buff=0)
            else:
                # t[1].set_color(TEAL)
                t[1].set_width(
                    self.ha.get_width()*(c[ni][1]/max_value),
                    stretch=True,
                )
                t[1].next_to(dfln[ni].get_center(),RIGHT,buff=0)
        
        return AnimationGroup(*[
            MoveToTarget(t) for t in state
        ])


    def find_id_target(self, list_targets, mob):
        id = mob.id[0]
        for l in list_targets:
            # print(f"l = {l}\t id = {id}")
            if l[0] == id:
                return l[1]


    def set_axes(self, ha):
        self.va = vertical_axes = Line(DOWN*3,UP*3)
        self.ha = ha
        # self.ha = horizontal_axes = Line(LEFT*3,RIGHT*3)
        self.va.next_to(ha,LEFT,buff=0)
        self.va.shift(UP*self.va.get_height()/2)
        self.ha.fade(1)

    
    def get_max_values(self):
        return [v[0][1] for v in self.values]
