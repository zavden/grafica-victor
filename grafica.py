from manimlib import *
from .imports import (
    SetLinesNumbers,
    SortCharPlot,
    get_data_from_csv
)

class TestGraph(Scene):
    def construct(self):
        step_values, coords = get_data_from_csv("Libro1formato")
        ha = Line(ORIGIN,RIGHT*FRAME_WIDTH*0.7)
        ha.move_to(ORIGIN)
        ha.to_edge(RIGHT)
        ha.to_edge(DOWN)
        ha.shift(UP)
        bar_chart = SortCharPlot(ha,"Libro1formato")
        self.bar_chart_height = bar_chart.get_height()
        max_values = bar_chart.get_max_values()
        initial_val = max_values[0]
        self.sdata = initial_val
        self.set_main_numberline()
        lines = self.get_start_lines()

        self.add(lines,bar_chart)
        self.wait(0.1)
        for j,i in enumerate(max_values[1:]):
            t = Text("%g"%step_values[j+1]).to_corner(DR)
            self.add(t)
            self.play(
                lines.set_next_value(self.nl, i),
                bar_chart.transform_to_next_value(j+1)
            )
            self.wait(0.5)
            self.remove(t)


    def set_main_numberline(self):
        nl = NumberLine(
            x_range=[0,self.sdata,1],
            width=FRAME_WIDTH*0.7,
            include_ticks=False,
        )
        nl.to_edge(RIGHT)
        nl.to_edge(DOWN)
        nl.shift(UP)
        nl.fade(1)
        self.nl = nl
        self.add(nl)

    def get_start_lines(self):
        group = SetLinesNumbers(
            self.nl,
            self.sdata,
            break_up_value=1.5,
            lines_height=self.bar_chart_height,
        )
        return group



class TestPlot(Scene):
    def construct(self):
        bc = SortCharPlot()

        self.add(bc)

        self.play(
            bc.transform_to_next_value(1)
        )
        self.wait()
        self.play(
            bc.transform_to_next_value(2)
        )
        self.wait()
        self.play(
            bc.transform_to_next_value(3)
        )
        self.wait()
        self.play(
            bc.transform_to_next_value(4)
        )
        self.wait()